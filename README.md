# BT-locker



###### PC

1. UI

   ![1542266611522](./doc/assets/1542266611522.png)
  - TextBlock에 블루투스 장치의 `고유 ID`를 입력하고 버튼을 클릭하여 실행함

  - Timer로 지정된 시간에 로그아웃이 될수있도록 설정가능(최소 설정가능 시간 5초)

    ---

2. Algorithm

   ![1542288346077](./doc/assets/1542288269348.png)

   - No Devices
     어플리케이션이 시작했으나 주변 장치를 찾고있지 않음.
   - Detecting
     3초간 설정한 입력된 주변기기를 찾기 시작. 찾기가 실패한경우 서비스 중단후, 사용자에게 입력 재확인 요구
   - Connected
     블루투스 신호의 세기가 기준치 이하인 상태
   - Disconnected
     블루투스 신호의 세기가 기준치 이상인 상태
   - Shutdown
     Disconnect statment 에서 지정된 시간이 경과하여 거리가 충분히 멀어졌다고 판단하고, 로그아웃/모니터 절전모드로 변경

---

###### Android Smartphone

1. UI

   ![1542262288219](./doc/assets/1542262288219.png)

- Generate 버튼을 터치하여 Smartphone의 `장치ID`를 생성함, 향후 `장치 ID`는 언제든지 바뀔수 있음.
  설치 후 최초 실행시에는 `고유 ID`가 없으므로 첫 실행시 필수 절차로 행해주어야하며, 이후 `고유 ID`는 자동저장됨.
- BroadCast 버튼을 스위치하여 BLE 기능을 ON/OFF 할 수 있음. 

   ---