package com.example.morning.bt_locker;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.RandomStringUtils;

import java.nio.charset.Charset;
import java.security.SecureRandomSpi;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter mBTAdapter;

    private Switch broadcastSW;

    private BroadcastReceiver mBTReceiver;

    private ServiceConnection conn;
    private AdvertiserService mService;

    private SharedPreferences appData;
    private String keyString;

    @FunctionalInterface
    interface Func {
        String generate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBTAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE))
                .getAdapter();

        if(mBTAdapter == null) { showErrorText(R.string.bt_not_supported); }
        if(!mBTAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        }
        if(mBTAdapter.isMultipleAdvertisementSupported()){
            setupAdvertiser();
        } else{
            showErrorText(R.string.bt_ads_not_supported);
        }

        conn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                AdvertiserService.LocalBinder lBinder = (AdvertiserService.LocalBinder) service;
                mService = lBinder.getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }
        };

        Func Keygen;
        {
            Keygen = () -> {
                int length = 8;
                boolean useLetters = true;
                boolean useNumbers = true;
                return RandomStringUtils.random(length, useLetters, useNumbers);
            };
        }

        Button genBtn = findViewById(R.id.key_genButton);
        genBtn.setOnClickListener(v -> {
            keyString = Keygen.generate();

            genBtn.setText(keyString);

            if(mService.running){
                mService.changeManufacturerData(keyString);
            }

            saveAppData();
        } );

        ImageButton lockBtn = findViewById(R.id.imageButton);
        lockBtn.setEnabled(false);
        lockBtn.setOnClickListener( v -> {
            if(genBtn.isEnabled()){
                genBtn.setEnabled(false);
            } else {
                genBtn.setEnabled(true);
            }
        });

        broadcastSW = findViewById(R.id.advertise_switch);
        broadcastSW.setOnCheckedChangeListener( (buttonView, isChecked) -> {
            if(isChecked) {
                if(!mBTAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
                }
                showErrorText(R.string.start_broadcast);
                startAdvertising();
            } else {
                showErrorText(R.string.stop_broadcast);
                stopAdvertising();
            }
        } );

        appData = getSharedPreferences("appData", MODE_PRIVATE);
        boolean keySaved = appData.getBoolean("SAVE_KEY_DATA", false);
        keyString = appData.getString("KEY", "");

        if(keySaved) {
            genBtn.setText(keyString);
            genBtn.setEnabled(false);
            lockBtn.setEnabled(true);
            broadcastSW.setChecked(true);
        }

    }

    @Override
    protected void onStop() {
        saveAppData();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if(broadcastSW.isChecked() && !mBTAdapter.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        }

    }

    @Override
    protected void onDestroy() {
        stopAdvertising();

        saveAppData();

        finish();
        super.onDestroy();
    }

    private void saveAppData() {
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean("SAVE_KEY_DATA", true);
        editor.putString("KEY", keyString);
        editor.apply();
    }

    private void setupAdvertiser(){
        mBTReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int errorCode = intent.getIntExtra(AdvertiserService.ADVERTISING_FAILED_EXTRA_CODE, -1);

                broadcastSW.setChecked(false);

                String errorMessage = getString(R.string.start_error_prefix);
                switch (errorCode) {
                    case AdvertiseCallback.ADVERTISE_FAILED_ALREADY_STARTED:
                        errorMessage += " " + getString(R.string.start_error_already_started);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_DATA_TOO_LARGE:
                        errorMessage += " " + getString(R.string.start_error_too_large);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_FEATURE_UNSUPPORTED:
                        errorMessage += " " + getString(R.string.start_error_unsupported);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_INTERNAL_ERROR:
                        errorMessage += " " + getString(R.string.start_error_internal);
                        break;
                    case AdvertiseCallback.ADVERTISE_FAILED_TOO_MANY_ADVERTISERS:
                        errorMessage += " " + getString(R.string.start_error_too_many);
                        break;
                    case AdvertiserService.ADVERTISING_TIMED_OUT:
                        errorMessage = " " + getString(R.string.advertising_timeout);
                        break;
                    default:
                        errorMessage += " " + getString(R.string.start_error_unknown);
                }

                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
            }
        };
    }

    private void startAdvertising(){
        Intent advertiseIntent = new Intent(this, AdvertiserService.class);
        advertiseIntent.putExtra("KEY", keyString);

        startService(advertiseIntent);
        bindService(advertiseIntent, conn, Context.BIND_AUTO_CREATE);
    }

    private void stopAdvertising(){
        unbindService(conn);
        Intent advertiseIntent = new Intent(this, AdvertiserService.class);
        stopService(advertiseIntent);
    }

    private void showErrorText(int messageId) {
        TextView view = findViewById(R.id.error_textview);
        view.setText(getString(messageId));
    }

}
