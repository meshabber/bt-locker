﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BLE_FromUWP;

using Windows.Devices.Bluetooth.Advertisement;
using Windows.Storage.Streams;

using Xceed.Wpf.Toolkit;

namespace WpfApp2
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        private BLE_AdvertisementListener listener;
        internal static MainWindow main;

        
        public MainWindow()
        {
            listener = new BLE_AdvertisementListener();

            InitializeComponent();
            main = this;
            ConnectString.Text = Properties.Settings.Default.KeyString;
        }
               
        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.Save();

            e.Cancel = true;
            this.Visibility = Visibility.Collapsed;
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            if (ConnectString.Text.Length <= 7) return;

            BluetoothLEManufacturerData filter;

            string keyString = ConnectString.Text;
            var data = Encoding.UTF8.GetBytes(keyString);
            var writer = new DataWriter(); writer.WriteBytes(data);
            
            filter = new BluetoothLEManufacturerData
            {
                CompanyId = 0x6865,
                Data = writer.DetachBuffer()
            };

            listener.AdvertisementFilter(filter);
            
            var str = string.Format("CompanyID=0x{0}, Data={1} \n",
                filter.CompanyId.ToString("X"),
                Encoding.UTF8.GetString(data));
            InputCheck.Text = str;

            Properties.Settings.Default.KeyString = keyString;

            listener.Start();
        }

        private void ToggleButton_UnChecked(object sender, RoutedEventArgs e)
        {
            listener.Stop();
        }

        private void Timer_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TimeSpan? delay = Timer.Value;

            listener.TimerReset(delay);       
        }
    }


}
