﻿using System;

/* UWP Bluetooth modules */
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Storage.Streams;

using WpfApp2;

namespace BLE_FromUWP {
    class BLE_AdvertisementListener {
        private BluetoothLEAdvertisementWatcher watcher;
        private DateTimeOffset last_timestamp;

        private System.Timers.Timer timer;

        public BLE_AdvertisementListener () {
            watcher = new BluetoothLEAdvertisementWatcher ();

            Console.WriteLine ("BLE_AdvertisementListener adversement watcher create");

            SetSignalStrengthFilter (-90, -125);
        }

        public void Start () {
            watcher.Received += OnAdvertisementReceived;
            watcher.Stopped += OnAdvertisementWatcherStopped;

            watcher.Start ();
            TimerSetup (TimeSpan.FromSeconds (5));
        }

        public void Stop () {
            if (timer != null) timer.Dispose ();

            watcher.Stop ();

            watcher.Received -= OnAdvertisementReceived;
            watcher.Stopped -= OnAdvertisementWatcherStopped;
        }

        private void TimerSetup (TimeSpan time) {
            timer = new System.Timers.Timer () {
                Interval = time.TotalMilliseconds,
                AutoReset = true
            };
            timer.Elapsed += async (s, e) => {
                SysCtlFromWin32.JustLock ();

                await MainWindow.main.Dispatcher.InvokeAsync (new Action (() => {
                    string str3 = string.Format ("[{0}]  Timer Trigered \n",
                        DateTime.Now.ToString ("hh\\:mm\\:ss\\.fff"));

                    MainWindow.main.ListenerOutput.AppendText (str3);
                }));
            };
        }

        public void TimerReset (TimeSpan? time) {
            if (timer != null) timer.Dispose ();

            timer = new System.Timers.Timer () {
                Interval = time.Value.TotalMilliseconds,
                AutoReset = true
            };
            timer.Elapsed += async (s, e) => {
                SysCtlFromWin32.JustLock ();

                await MainWindow.main.Dispatcher.InvokeAsync (new Action (() => {
                    string str3 = string.Format ("[{0}]  Timer Trigered \n",
                        DateTime.Now.ToString ("hh\\:mm\\:ss\\.fff"));

                    MainWindow.main.ListenerOutput.AppendText (str3);
                }));
            };
        }

        public void SetSignalStrengthFilter (Int16 inRange, Int16 outRange) {
            watcher.SignalStrengthFilter.InRangeThresholdInDBm = inRange;
            watcher.SignalStrengthFilter.OutOfRangeThresholdInDBm = outRange;
            watcher.SignalStrengthFilter.OutOfRangeTimeout = TimeSpan.FromSeconds (3);
        }

        public void AdvertisementFilter (BluetoothLEManufacturerData filter) {
            Stop ();

            watcher.AdvertisementFilter.Advertisement.ManufacturerData.Clear ();
            watcher.AdvertisementFilter.Advertisement.ManufacturerData.Add (filter);

            var str = string.Format ("reset filter[0x{0}:{1}]\n",
                filter.CompanyId.ToString ("X"),
                GetStringFromIBuffer (filter.Data));
            MainWindow.main.ListenerOutput.AppendText (str);

            Start ();
        }

        private string GetStringFromIBuffer (IBuffer buf) {
            var data = new byte[buf.Length];
            using (var reader = DataReader.FromBuffer (buf)) {
                reader.ReadBytes (data);
            }

            return System.Text.Encoding.UTF8.GetString (data);
        }

        private async void OnAdvertisementReceived (BluetoothLEAdvertisementWatcher watcher, BluetoothLEAdvertisementReceivedEventArgs eventArgs) {
            last_timestamp = eventArgs.Timestamp;

            BluetoothLEAdvertisementType advertisementType = eventArgs.AdvertisementType;

            Int16 rssi = eventArgs.RawSignalStrengthInDBm;
            string localName = eventArgs.Advertisement.LocalName;

            string manufacturerDataString = "";
            var manufacturerSections = eventArgs.Advertisement.ManufacturerData;

            // Must be Receieved Only one device
            if (manufacturerSections.Count > 0) {
                // Only print the first one of the list
                var manufacturerData = manufacturerSections[0];
                var data = GetStringFromIBuffer (manufacturerData.Data);
                // Print the company ID + the raw data in hex format
                manufacturerDataString = string.Format ("0x{0}: {1}",
                    manufacturerData.CompanyId.ToString ("X"),
                    data);
                //BitConverter.ToString(data));
            }

            var eventTime = last_timestamp.ToString ("[hh\\:mm\\:ss\\.fff]");
            var adType = advertisementType.ToString ();

            string debugStr = string.Format ("\n [{0}]: ", eventTime);
            debugStr += string.Format ("rssi={0}, data=[{1}]",
                rssi.ToString (), manufacturerDataString);

            // string str = string.Format ("[{0}]: type={1}, rssi={2}, name={3}, manufacturerData=[{4}]",
            //     last_timestamp.ToString ("hh\\:mm\\:ss\\.fff"),
            //     advertisementType.ToString (),
            //     rssi.ToString (),
            //     localName,
            //     manufacturerDataString);

            // string str2 = string.Format ("rssi={0}, name={1}, manufacturerData=[{2}] \n",
            //     rssi.ToString (),
            //     localName,
            //     manufacturerDataString);

            var connStatus = "";
            if (rssi == -127) {
                debugStr += "===TimerStart===\n";
                timer.Start ();
                connStatus = "Disconnect";
            } else {
                // debugStr += "===TimerStop===\n";
                timer.Stop ();
                connStatus = "Connect";
            }

            await MainWindow.main.Dispatcher.InvokeAsync (new Action (() => {
                MainWindow.main.ListenerOutput.AppendText (debugStr);
                MainWindow.main.lbl_status.Content = connStatus;
            }));
        }

        private async void OnAdvertisementWatcherStopped (BluetoothLEAdvertisementWatcher watcher, BluetoothLEAdvertisementWatcherStoppedEventArgs eventArgs) {
            await MainWindow.main.Dispatcher.InvokeAsync (new Action (() => {
                MainWindow.main.ListenerOutput.AppendText ("watcher stoped \n");
                MainWindow.main.lbl_status.Content = "Disconnect";
            }));
        }
    }
}